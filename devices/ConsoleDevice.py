import time

import datetime
import usb

import threading

from src.connector.ConnectorConsts import RELOAD_CONSOLE_STATE
from src.devices.ConsoleConfig import *
from src.devices.Settings import *
from src.devices.SimulateKeyboard import SendInputKeyboard
from src.utils.debug import printInfo


class ConsoleDevice:
    def __init__(self, threads, processIsTerminate, transportCallback):
        self.threads = threads
        self._isRunning = True
        self.processIsTerminate = processIsTerminate
        self.cardThread = None
        self.consoleThread = None
        self.__handleConsole = None
        self.__isUsbDeviceConnect = False
        self.__transportCallback = transportCallback

    def startSerive(self):
        self.consoleThread = threading.Thread(target=self.__start, name="consoleServiceThread")
        self.consoleThread.start()

        self.cardThread = threading.Thread(target=self.__startReadConsoleDevice, name="consoleDeviceThread")
        self.cardThread.start()

    def __start(self):
        i = 0
        while self._isRunning:
            i += 1
            try:
                if self.__isUsbDeviceConnect == False:
                    self.__isUsbDeviceConnect = True
                    self.__initUsbDevice()
                    self.__transportCallback("stableDevice")
            except Exception as e:
                self.__isUsbDeviceConnect = False
                time.sleep(3)
            time.sleep(5)

    def __initUsbDevice(self):
        """
        Find and open a USB HID device.
        """

        self.__lastStateKeyboard = lastStateKeyboard
        self.__endpoint = 0x1

        self.__initConsole()

    def __initConsole(self):
        self.__deviceConsole = self.__getDevice(USB_CONSOLE_VENDOR_ID, USB_CONSOLE_DEVICE_ID)
        self.__handleConsole = self._openDevice(self.__deviceConsole)

    def _openDevice(self, device):
        pass

    def __getDevice(self, vendor_id, device_id):
        """
        Searches the USB buses for a device matching the given vendor and device IDs.
        Returns a usb.Device, or None if the device cannot be found.
        """

        busses = usb.busses()

        for bus in busses:
            devices = bus.devices
            for device in devices:
                print("device - {idVendor:", device.idVendor, "idProduct:", device.idProduct, "}")
                if device.idVendor == vendor_id and device.idProduct == device_id:
                    print("device found:::", device)
                    return device
        #
        return None

    def __getDeviceFromCore(self, vendor_id, device_id):

        all = usb.core.find(find_all=True)
        #
        for device in all:
            if device.idVendor == vendor_id and device.idProduct == device_id:
                print "usb.find", device
                return device

    def __startReadConsoleDevice(self):
        self.__work()

    def __work(self):
        answerKeyboard = None
        data = None
        data2 = None
        t = 0
        while self.processIsTerminate.value == 0 and self._isRunning:
            if self.__handleConsole:
                data = None
                try:
                    data = self.__handleConsole.bulkWrite(0x2, keyboard[self.__lastStateKeyboard], 0)
                    t += 1
                    if data:
                        data2 = self.__handleConsole.interruptRead(0x81, 8, 500)
                        pass
                    if data2:
                        k = [data2[0], data2[1], data2[2], data2[3], data2[4], data2[5]]

                        if answerKeyboard == k:
                            pass
                        else:
                            answerKeyboard = k
                            self.__transportKeyDown(answerKeyboard)
                except Exception as e:
                    if not self.__isUsbDeviceConnect:
                        self.__transportCallback("errorDevice")
                    self.__isUsbDeviceConnect = False
                    time.sleep(3)

            time.sleep(0.05)
        print("--stop thread--")

    def testClickKey(self, t):
        if t % 3 == 0:
            self.__sendGlobalKeyToFlash(12)
        if t % 3 == 1:
            self.__sendGlobalKeyToFlash(11)
        if t % 3 == 2:
            self.__sendGlobalKeyToFlash(10)
        if t % 300 == 0:
            self.__sendGlobalKeyToFlash(17)

    def changeButtons(self, keys):
        if len(keys) <> 23:
            keys = self.__lastStateKeyboard
        self.__lastStateKeyboard = keys;

    def __transportKeyDown(self, arrBytes):
        count = 2
        for key in keyAnswerCode:
            keyCodes = keyAnswerCode[key]
            i = 0
            r = 0
            for i in range(0, count):
                if arrBytes[i] == keyCodes[i]:
                    r += 1

            if r == count:
                self.__sendGlobalKeyToFlash(key)


    def __sendGlobalKeyToFlash(self, key):
        tArr = {1:"p", 2:"1", 3:"3", 4:"5", 5:"7", 6:"9", 7:"s", 8:"i", 9:"c", 10:"a", 11:"b", 12:"m", 17:"Return"}
        # SendInputKeyboard(tArr.get(key))
        tKey = "KEY_" +  tArr.get(key)
        print tKey
        self.__transportCallback(tKey)
        # SendInputKeyboard(tArr.get(key))

    def destroy(self):
        printInfo("Wait to close Console")
        print("destroy CONSOLE device:::", datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'))
        self._isRunning = False
        try:
            #self.__handleConsole.bulkWrite(0x2, self.__config.offKeyboard, 0)
            self.__handleConsole.releaseInterface()

            del self.__deviceConsole
            del self.__handleConsole
            print("remove ok __handleConsole__", self.cardThread)
        except:
            pass

        self.cardThread = None