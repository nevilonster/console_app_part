from src.devices.Settings import *

keyboard = dict()
default = "0,0,0,0,0,0,0,0,0,0,0,0"
lastStateKeyboard = default
keyAnswerCode = dict()

# init lighting keys
def __initKeys():
    # pay, 1 line, 3 line, 5 line, 7 line, 9 line, start
    #          info, change, auto, menu, bet
    # keyboard = [0, 0, 0, 0, 0, 0, 0,
    #               0, 0, 0, 0, 0]

    offKeyboard = [0, 0]
    onlyStart = [1, 0]
    onlyInfo = [2, 0]


    # default = "1,1,1,1,1,0,0,0,1,0,0,1"
    # default = "1,1,1,1,1,0,0,0,0,0,0,0"

    keyboard[__setKeyKeyboard([], default)] = [0, 0]

    __set357KeyCombination([], 0)
    __set357KeyCombination([KEY_LINE_9], 1)
    __set357KeyCombination([KEY_LINE_1], 2)
    __set357KeyCombination([KEY_LINE_9, KEY_LINE_1], 3)

    __set357KeyCombination([KEY_AUTO], 32)
    __set357KeyCombination([KEY_AUTO, KEY_LINE_9], 33)
    __set357KeyCombination([KEY_AUTO, KEY_LINE_1], 34)
    __set357KeyCombination([KEY_AUTO, KEY_LINE_9, KEY_LINE_1], 35)

    __set357KeyCombination([KEY_BET], 64)
    __set357KeyCombination([KEY_BET, KEY_LINE_9], 65)
    __set357KeyCombination([KEY_BET, KEY_LINE_1], 66)
    __set357KeyCombination([KEY_BET, KEY_LINE_9, KEY_LINE_1], 67)


    __set357KeyCombination([KEY_BET, KEY_AUTO], 96)
    __set357KeyCombination([KEY_BET, KEY_AUTO, KEY_LINE_9], 97)
    __set357KeyCombination([KEY_BET, KEY_AUTO, KEY_LINE_1], 98)
    __set357KeyCombination([KEY_BET, KEY_AUTO, KEY_LINE_9, KEY_LINE_1], 99)

    __set357KeyCombination([KEY_PAY], 128)
    __set357KeyCombination([KEY_PAY, KEY_LINE_9], 129)
    __set357KeyCombination([KEY_PAY, KEY_LINE_1], 130)
    __set357KeyCombination([KEY_PAY, KEY_LINE_9, KEY_LINE_1], 131)

    __set357KeyCombination([KEY_PAY, KEY_AUTO], 160)
    __set357KeyCombination([KEY_PAY, KEY_AUTO, KEY_LINE_9], 161)
    __set357KeyCombination([KEY_PAY, KEY_AUTO, KEY_LINE_1], 162)
    __set357KeyCombination([KEY_PAY, KEY_AUTO, KEY_LINE_9, KEY_LINE_1], 163)

    __set357KeyCombination([KEY_PAY, KEY_BET], 192)
    __set357KeyCombination([KEY_PAY, KEY_BET, KEY_LINE_9], 193)
    __set357KeyCombination([KEY_PAY, KEY_BET, KEY_LINE_1], 194)
    __set357KeyCombination([KEY_PAY, KEY_BET, KEY_LINE_9, KEY_LINE_1], 195)

    __set357KeyCombination([KEY_PAY, KEY_AUTO, KEY_BET], 224)
    __set357KeyCombination([KEY_PAY, KEY_AUTO, KEY_BET, KEY_LINE_9], 225)
    __set357KeyCombination([KEY_PAY, KEY_AUTO, KEY_BET, KEY_LINE_1], 226)
    __set357KeyCombination([KEY_PAY, KEY_AUTO, KEY_BET, KEY_LINE_9, KEY_LINE_1], 227)
    #------------------------end block 127--------------------------

# data2 = __handleConsole.interruptRead(0x81, 8, 500)  ->  [a, b, 103, 35, 54, 21]
# get key by data2
def __initConsoleAnswerKeyboard():

    keyAnswerCode[KEY_PAY] =     [251, 255, 103, 35, 54, 21]
    keyAnswerCode[KEY_LINE_1] =  [191, 255, 103, 35, 54, 21]
    keyAnswerCode[KEY_LINE_3] =  [255, 254, 103, 35, 54, 21]
    keyAnswerCode[KEY_LINE_5] =  [255, 253, 103, 35, 54, 21]
    keyAnswerCode[KEY_LINE_7] =  [255, 251, 103, 35, 54, 21]
    keyAnswerCode[KEY_LINE_9] =  [127, 255, 103, 35, 54, 21]
    keyAnswerCode[KEY_START] =   [255, 191, 103, 35, 54, 21]
    keyAnswerCode[KEY_INFO] =    [255, 223, 103, 35, 54, 21]
    keyAnswerCode[KEY_CHANGE] =  [255, 247, 103, 35, 54, 21]
    keyAnswerCode[KEY_AUTO] =    [254, 255, 103, 35, 54, 21]
    keyAnswerCode[KEY_MENU] =    [255, 239, 103, 35, 54, 21]
    keyAnswerCode[KEY_BET] =     [255, 127, 103, 35, 54, 21]

# Combination keys: 3, 5, 7
def __set357KeyCombination(keys, secondByte):
    arr =  [
        [],  #0-16
        [KEY_LINE_7], #16-32
        [KEY_LINE_5], #32-48
        [KEY_LINE_5, KEY_LINE_7], #48-64
        [KEY_LINE_3], #64-80
        [KEY_LINE_3, KEY_LINE_7], #80-96
        [KEY_LINE_3, KEY_LINE_5], #96-112
        [KEY_LINE_3, KEY_LINE_5, KEY_LINE_7]] #112-128

    for key in keys:
        for el in arr:
            el.append(key)

    pos = 0
    for el in arr:
        pos = __setInfoMenuChange(el, pos, secondByte)

def __setInfoByCombination(arr, i, secondByte):
    i = i - 1
    for comb in arr:
        i = i+1
        t = __setKeyKeyboard(comb, default)
        keyboard[t] = [i, secondByte]
    return i

# Combination keys: start, info, menu, change
def __setInfoMenuChange(keys, pos, secondByte):
    arr = [[], [KEY_START], [KEY_INFO], [KEY_START, KEY_INFO], [KEY_MENU],
           [KEY_START, KEY_MENU], [KEY_INFO, KEY_MENU], [KEY_START, KEY_INFO, KEY_MENU],
           [KEY_CHANGE], [KEY_START, KEY_CHANGE], [KEY_INFO, KEY_CHANGE],
           [KEY_START, KEY_INFO, KEY_CHANGE], [KEY_MENU, KEY_CHANGE], [KEY_START, KEY_MENU, KEY_CHANGE],
           [KEY_INFO, KEY_MENU, KEY_CHANGE], [KEY_START, KEY_INFO, KEY_MENU, KEY_CHANGE]]


    for arrKeys in arr:
        for key in keys:
            arrKeys.append(key);
    pos = __setInfoByCombination(arr, pos, secondByte)

    return pos + 1

def __setKeyKeyboard(keys, str):
    arr = str.split(",")
    for key in keys:
        arr[(key - 1)] = '1' # key - 1 -> key started from 1, arr start from 0

    str = ','.join(arr)
    return str

def initConfigConsoleInitialize():
    __initConsoleAnswerKeyboard()
    __initKeys()