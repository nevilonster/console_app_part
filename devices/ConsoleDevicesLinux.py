from src.devices.ConsoleDevice import ConsoleDevice


class ConsoleDevicesLinux(ConsoleDevice):
    def __init__(self, threads, processIsTerminate, transportCallback):
        ConsoleDevice.__init__(self, threads, processIsTerminate, transportCallback)

    def _openDevice(self, device):
        """
        Opens and claims the specified device. Returns a usb.DeviceHandle
        Also attempts to detach the kernel's driver from the device, if necessary.
        """

        handle = device.open()
        try:
            handle.detachKernelDriver(0)
        except:
            print("detach kernel driver ")

        return handle

if __name__ == '__main__':
    threads = []
    consoleDevices = ConsoleDevicesLinux(threads)
    consoleDevices.start()