from src.devices.ConsoleDevice import ConsoleDevice


class ConsoleDevicesWindows(ConsoleDevice):
    def __init__(self, threads, processIsTerminate, transportCallback):
        ConsoleDevice.__init__(self, threads, processIsTerminate, transportCallback)

    def _openDevice(self, device):
        """
        Opens and claims the specified device. Returns a usb.DeviceHandle
        Also attempts to detach the kernel's driver from the device, if necessary.
        """

        handle = device.open()
        handle.setConfiguration(1)
        handle.claimInterface(0)

        return handle

if __name__ == '__main__':
    threads = []
    consoleDevices = ConsoleDevicesWindows(threads)
    consoleDevices.start()