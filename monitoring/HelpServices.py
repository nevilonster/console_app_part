# -*- coding: utf-8 -*-
import os
import platform
from threading import Thread

import psutil
import time

from src.com import Config


class KillCopyService():
    def check(self):
        myPid = os.getpid()
        appName = "ConsoleService"

        for proc in psutil.process_iter():
            if proc.pid == myPid:
                appName = proc.name().__str__()

        print(appName)

        if Config.SYSTEM_OS == Config.WINDOWS_OS:
            appName += ".exe"

        apName = "onsoleS"
        for proc in psutil.process_iter():
            # if proc.pid <> myPid and proc.name() == appName:
            if proc.pid <> myPid and proc.name().find(apName) <> -1:
                print "kill proc:", str(proc.pid)
                proc.kill()

class FlashRestarterService(Thread):
    def __init__(self):
        Thread.__init__(self)
        self._isRunning = True
        self._pid = -1
        self.appName = "StandAlone"

    def run(self):
        self.killFlash()
        self.restartFlash()


    def restartFlash(self):
        pass

    def killFlash(self):
        # print(psutil.Process(self._pid))
        if self._pid == -1 or psutil.Process(self._pid) is None:
            for proc in psutil.process_iter():
                p = psutil.Process(proc.pid)
                if proc.name().find(self.appName) <> -1:
                    self._pid = proc.pid
                    proc.kill()
                break
        if self._pid <> -1:
            self.infoByPid(self._pid)

    def infoByPid(self, pid):
        p = psutil.Process(pid)
        if p.name() == self.appName:
            print ("name:", p.name(), " - :",
                   "cpu_percent:", p.cpu_percent(),"memory_info:", p.memory_info().vms)

    def destroy(self):
        self._isRunning = False