from src.technicalService.services.BaseService import BaseService


class VersionService(BaseService):
    def __init__(self, id):
        BaseService.__init__(self, id)
        self.version = "0.00"

    def setValue(self, value):
        self.version = value

    def getValue(self):
        return self.version