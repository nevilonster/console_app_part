import os
import subprocess
from time import sleep

from pyupdater.client.updates import Restarter

from src.com import Config
from src.monitoring.HelpServices import FlashRestarterService
from src.technicalService.services.BaseService import BaseService
import psutil

class SystemService(BaseService):
    def __init__(self, id):
        BaseService.__init__(self, id)
        # self.commands[1] = self.restartClient
        self.commands[1] = self.reboot
        self.commands[2] = self.shutdownComputer

    def call(self, param):
        self._callCommandByParam(param)

    def restartClient(self):
        print "restart client"
        flashName = "StandAlone"
        for proc in psutil.process_iter():
            p = psutil.Process(proc.pid)
            if proc.name().find(flashName) <> -1:
                proc.kill()

        sleep(2)

        command = "/home/client/card"
        p=subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()

    def reboot(self):
        print "restart computer"
        if Config.SYSTEM_OS == Config.LINUX_OS:
            command = "/sbin/reboot"
        else:
            command = "shutdown -r -t 1"

        subprocess.call(command, shell=True)

    def shutdownComputer(self):
        print "shutdownComputer"
        if Config.SYSTEM_OS == Config.LINUX_OS:
            command = "/sbin/shutdown -s"
        else:
            command = "shutdown -h -t 1"
        subprocess.call(command, shell=True)