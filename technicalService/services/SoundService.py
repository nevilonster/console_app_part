from src.com import Config
from src.technicalService.services import TestImport
from src.technicalService.services.BaseService import BaseService

import sys, importlib

class SoundService(BaseService):

    def __init__(self, id):
        BaseService.__init__(self, id)
        # self.commands = dict()
        self.idSound = 3

        self.commandsLinux = dict()
        self.commandsWin = dict()

        TestImport.tryImport()
        self.soundMixer = self.getMixer()

    def call(self, param):
        self.setValue(param)

    def __call(self, param):
        print(self.soundMixer)
        self.soundMixer.setVolume(param)

    def getMixer(self):
        lib = None
        if Config.SYSTEM_OS == Config.LINUX_OS:
            lib = importlib.import_module("src.technicalService.services.soundModule.LinuxSoundMixer")
        if Config.SYSTEM_OS == Config.WINDOWS_OS:
            lib = importlib.import_module("src.technicalService.services.soundModule.WindowsSoundMixer")

        return lib.SoundMixer()

    def setValue(self, value):
        print "set sound id:", value
        self.idSound = value
        self.__call(int(value))

    def getValue(self):
        return self.idSound

