from ctypes import cast, POINTER
from comtypes import CLSCTX_ALL, CoCreateInstance
from pycaw.pycaw import AudioUtilities, IAudioEndpointVolume, CLSID_MMDeviceEnumerator

from src.com.Config import IS_WINDOWS_XP
from src.technicalService.services.soundModule.BaseSoundMixer import BaseSoundMixer
from src.utils.debug import printInfo

import win32api

class SoundMixer(BaseSoundMixer):
    def __init__(self):
        BaseSoundMixer.__init__(self)
        self.commands[1] = 6553
        self.commands[2] = 19660
        self.commands[3] = 32767
        self.commands[4] = 45873
        self.commands[5] = 65534


    def setVolume(self, value):

        print "set volume:", value
        from subprocess import call
        call(["C:\\nircmd\\nircmd", "setsysvolume", str(self.commands.get(value, 32767))])

        if True:
            return

        if IS_WINDOWS_XP:
            pass
        else:
            devices = AudioUtilities.GetSpeakers()
            interface = devices.Activate(
                IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
            volume = cast(interface, POINTER(IAudioEndpointVolume))

            volume.SetMasterVolumeLevel(self.commands.get(value, -10), None)
            printInfo("restore sound:" + str(self.commands.get(value, -10)))
