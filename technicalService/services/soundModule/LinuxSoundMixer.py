from src.technicalService.services.soundModule.BaseSoundMixer import BaseSoundMixer
import alsaaudio

from src.utils.debug import printInfo


class SoundMixer(BaseSoundMixer):
    def __init__(self):
        BaseSoundMixer.__init__(self)
        self.commands[1] = 10
        self.commands[2] = 30
        self.commands[3] = 50
        self.commands[4] = 70
        self.commands[5] = 100

    def setVolume(self, value):
        m = alsaaudio.Mixer()
        m.setvolume(self.commands.get(value, 50))
        printInfo("restore sound:" + str(self.commands.get(value, 50)))