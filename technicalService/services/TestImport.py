from src.utils.debug import printErr


def tryImport():
    try:
        from src.technicalService.services.soundModule import WindowsSoundMixer
    except:
        printErr("Windows not supported")

    try:
        from src.technicalService.services.soundModule import LinuxSoundMixer
    except:
        printErr("Linux not supported")