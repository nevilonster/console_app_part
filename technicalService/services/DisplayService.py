import os

from src.com import Config
from src.technicalService.services.BaseService import BaseService


class DisplayService(BaseService):
    def __init__(self, id):
        BaseService.__init__(self, id)
        self.commands[1] = "800x600_60"
        self.commands[2] = "1024x768_60"
        self.commands[3] = "1280x1024_60"

        self.commandsWin = dict()
        self.commandsWin[1] = dict()
        self.commandsWin[1][0] = 800
        self.commandsWin[1][1] = 600
        self.commandsWin[1][2] = 32
        self.commandsWin[2] = dict()
        self.commandsWin[2][0] = 1024
        self.commandsWin[2][1] = 768
        self.commandsWin[2][2] = 32
        self.commandsWin[3] = dict()
        self.commandsWin[3][0] = 1280
        self.commandsWin[3][1] = 1024
        self.commandsWin[3][2] = 32


    def call(self, param):
        if Config.SYSTEM_OS == Config.LINUX_OS:
            size = self.commands.get(param, None)
            if size is not None:
                command = "xrandr -s " + size
                os.popen(command)

        if Config.SYSTEM_OS == Config.WINDOWS_OS:
            size = self.commandsWin.get(param, None)
            if size is not None:
                from subprocess import call
                try:
                    call(["C:\\nircmd\\nircmd", "setdisplay", str(size[0]), str(size[1]), str(size[2])])
                except Exception as e:
                    print e

    def __setScreenSize(self, param):
        pass