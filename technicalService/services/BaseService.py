class BaseService():
    def __init__(self, id):
        self.commands = dict()
        self.id = id

    def call(self, param):
        pass

    def _callCommandByParam(self, param):
        command = self.commands.get(param, None)
        if command is not None:
            command()
        else:
            print "not supported command:", param

    def setValue(self, value):
        pass

    def getValue(self):
        return 0

    def getID(self):
        return self.id