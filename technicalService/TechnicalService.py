import importlib
import pickle
from time import sleep

from src.com import Config
from src.connector.ConnectorConsts import *
from src.technicalService.services.DisplayService import DisplayService

from src.technicalService.services.SystemService import SystemService

# DEFAULT_CONFIG = "1:3,2:0,3:0,4:0"
from src.technicalService.services.VersionService import VersionService
from src.utils.debug import printErr, printInfo
from src.technicalService.services.SoundService import SoundService

DEFAULT_CONFIG = ["0:1.00", "1:3", "2:0", "3:0", "4:0"]

class bcolors:
    WARNING = "\033[95m"
    ENDC = "\033[0m"

class TechnicalService:
    def __init__(self):
        self.isCardDepartment = False

        if Config.SYSTEM_OS == Config.WINDOWS_OS:
            self.datafile = "c:\client\datafile"
        if Config.SYSTEM_OS == Config.LINUX_OS:
            self.datafile = "/home/client/datafile"

        self.fileName = "config.txt"
        self.configVersion = "1.01"
        self.versionService = VersionService(0)


        self.soundService = SoundService(1)

        self.systemService = SystemService(2)
        self.displayService = DisplayService(3)


        self.services = dict()

        self.services[0] = self.versionService
        self.services[1] = self.soundService
        self.services[2] = self.systemService
        self.services[3] = self.displayService
        self.loadTypeDepartment()
        self.loadConfig()

    def loadTypeDepartment(self):
        # dep_type: 1 -> card, 2 -> check department
        try:
            f = open(self.datafile, "r")
            data = pickle.load(f)
            self.isCardDepartment = int(data.get("dep_type", None)) == 1
            f.close()
            printInfo("Type department: " + str(data.get("dep_type", None)))
        except Exception as e:
            printErr(self.datafile + " is not created, or corrupted " + str(e))
            printInfo("Type department: 1 (card at default)")

    def isCardDepartment(self):
        return self.isCardDepartment

    def loadConfig(self):
        isSave = False
        isDefault = False
        try:
            f = open(self.fileName, "r")
            data = f.read()
            print "loadConfig:", data
            f.close()
            settings = data.split(",")

            if settings.__len__() < 3:
                isDefault = True
        except:
            print "Load config failed"
            isDefault = True

        if isDefault:
            settings = DEFAULT_CONFIG
            isSave = True

        for setting in settings:
            id, value = setting.split(":")
            self.setConfig(int(id), value)

        # if self.versionService.getValue() <> self.configVersion:  #TODO add methods to update config file by version

        if isSave:
            self.saveConfig()



    def setConfig(self, id, value):
        service = self.services.get(id, None)
        if service is not None:
            if service.getID() == id:
                service.setValue(value)

    def saveConfig(self):
        try:
            f = open(self.fileName, "w")
            value = ""
            for idService in self.services:
                service = self.services.get(idService, None)
                value += str(service.getID()) + ":" + str(service.getValue()) + ","
                print service.getID(), service.getValue()
            value += "4:0"

            f.write(value)
            f.close()
        except Exception as e:
            print bcolors.WARNING + "Error write to file" + bcolors.ENDC, e

    def tryCommand(self, value):
        if value.find(KEY_DELIMITER) == -1:
            print "not found settings, command:", value
            return
        arr = value.split(KEY_DELIMITER)
        if len(arr) < 3:
            print "insufficiently parameters, command:", value
        serviceID = int(arr[1])
        command = int(arr[2])
        service = self.services.get(serviceID, None)
        if service is not None:
            service.call(command)
        else:
            print "service not found:", str(serviceID)
            print "command:", value