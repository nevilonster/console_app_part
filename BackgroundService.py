import multiprocessing
import os
import time
from threading import Thread

# from src.cardReader.ArduinCardReader import ArduinCardReader
from src.cardReader.CardReaderDeviceWindows import CardReaderDeviceWindows
from src.com.Config import LINUX_OS, WINDOWS_OS, setSystemOS
from src.log.Logger import MY_LOGGER

from src.monitoring.HelpServices import FlashRestarterService, KillCopyService
from src.connector.ConnectorConsts import *

from src.cardReader.CardReaderDeviceLinux import CardReaderDeviceLinux
from src.connector.SocketConnector import SocketConnector
from src.devices.ConsoleDevicesLinux import ConsoleDevicesLinux
from src.devices.ConsoleDevicesWindows import ConsoleDevicesWindows
from src.connector import ConnectorConsts
import platform

from src.devices.our.ArduinConsoleDevice import ArduinConsoleDevice
from src.devices.ConsoleConfig import initConfigConsoleInitialize
from src.technicalService.TechnicalService import TechnicalService
from src.utils.debug import printErr, printInfo


# TYPE_CONSOLE = "arduin"
TYPE_CONSOLE = "none"

class Main:
    def __init__(self):
        self.connector = None
        self.consoleDevices = None
        self.cardReader = None
        self.os = None
        self.portCardReader = 3
        self.process_isTerminate = multiprocessing.Value("d", 0)
        self.tProc = 0
        self.serialNumber = ""
        initConfigConsoleInitialize()

    def start(self):
        self.threads = []

        self.checkOS()
        self.technicalService = TechnicalService()
        self.connector = SocketConnector(self.threads)
        self.connector.init(2022, self.__listenKeysFromFlash)

        if self.technicalService.isCardDepartment:
            self.__startCardReader()
            self.startConsole()

    def startConsole(self):
        self.consoleDevices = self.getConsoleDevices()(self.threads, self.process_isTerminate, self.transportFromConsoleToFlash)
        self.consoleDevices.startSerive()

    def transportFromConsoleToFlash(self, value = None):
        if self.connector is not None:
            self.connector.transportFromConsoleToFlash(value)
        # arr = value.split("_")
        # if len(arr) > 1:
        #     value = TECHNICAL_CONNECTION + KEY_DELIMITER + str(2) + KEY_DELIMITER + arr[1]
        #     self.__listenKeysFromFlash(value)

        #

    def terminate(self):
        # self.isTerminate = True
        printErr("start exit process")
        self.process_isTerminate.value = 1
        self.tProc = 1
        if hasattr(self, "connector") and self.connector is not None:
            self.connector.destroy()

        if self.consoleDevices is not None:
            self.consoleDevices.destroy()

        if hasattr(self, "cardReader") and self.cardReader is not None:
            self.cardReader.terminate()

    def checkOS(self):
        self.os = platform.system()

        setSystemOS(self.os, platform.platform())

        printInfo(self.os)

    def getConsoleDevices(self):
        if TYPE_CONSOLE == "arduin":
            return ArduinConsoleDevice

        if self.os == WINDOWS_OS:
            return ConsoleDevicesWindows
        if self.os == LINUX_OS:
            return ConsoleDevicesLinux

        self.printOs()
        raise Exception(self.os + " is not supported. " + self.printOs())

    def __startCardReader(self, isCard = None, cardID = None, serialNumber = None):
        if self.process_isTerminate.value == 1:
            printInfo("CardReader Exit success")
            os._exit(0)
            return
        print "start card reader", isCard, cardID, serialNumber
        self.cardReader = self.getCardReader()(self.threads, self.process_isTerminate)
        self.cardReader.port = self.portCardReader
        self.cardReader.init(callback=self.__cardReaderCallback, isCard=isCard, cardID=cardID, serialNumber=serialNumber)

    def getCardReader(self):
        if TYPE_CONSOLE == "arduin":
            # return ArduinCardReader
            pass
        if self.os == WINDOWS_OS:
            return CardReaderDeviceWindows
        if self.os == LINUX_OS:
            return CardReaderDeviceLinux
        # self.printOs()
        raise Exception(self.os + " is not supported." + self.printOs())

    def __cardReaderCallback(self, value):
        if value == ConnectorConsts.UPDATE_CARD_AT_READER_STATE:
            self.sendChangeCardState()
        if value == ConnectorConsts.RELOAD_READER_STATE:
            cardID = self.cardReader.serialNumber
            isCard = self.cardReader.getCardReaderState()
            self.portCardReader = self.cardReader.port

            serialNumber = self.cardReader.serialNumber
            self.cardReader.destroy()
            del self.cardReader
            time.sleep(5)
            self.__startCardReader(cardID=cardID, isCard=isCard, serialNumber=serialNumber)
        if value == TERMINATE_THREAD:
            self.cardReader.destroy()
            del self.cardReader
            time.sleep(1)
            self.__startCardReader()

    def sendChangeCardState(self):
        self.serialNumber = self.cardReader.serialNumber
        res = ConnectorConsts.CHANGE_STATE_CARD_AT_CONSOLE + ConnectorConsts.KEY_DELIMITER + \
              str(self.cardReader.serialNumber) + ConnectorConsts.KEY_DELIMITER + str(self.cardReader.serialNumber)
        if self.connector is not None:
            self.connector.sendData(res)

    def __listenKeysFromFlash(self, value):
        print("listenKeysFromFlash:", value)
        params = ""
        if value.find(KEY_DELIMITER) == -1:
            key = value
        else:
            arr = value.split(KEY_DELIMITER)
            key = arr[0]
            params = arr[1]

            # key, params = value.split(KEY_DELIMITER)

        if key == ACTIVATE_CONNECTION:
            if self.connector is not None:
                self.connector.sendData(ACTIVATE_CONNECTION)
        if key == CHANGE_BUTTONS:
            self.__changeButtonAtConsole(params)
        if key == TECHNICAL_CONNECTION:
            self.technicalService.tryCommand(value)

    def __changeButtonAtConsole(self, keys):
        try:
            self.consoleDevices.changeButtons(keys)
        except:
            print("--not connection to keyboard--")

    def linux_distribution(self):
        try:
            return platform.linux_distribution()
        except:
            return "N/A"

    def printOs(self):
        return """
            Python version: %s
            dist: %s
            linux_distribution: %s
            system: %s
            machine: %s
            platform: %s
            uname: %s
            version: %s
            mac_ver: %s
            """ % (
            sys.version.split('\n'),
            str(platform.dist()),
            self.linux_distribution(),
            platform.system(),
            platform.machine(),
            platform.platform(),
            platform.uname(),
            platform.version(),
            platform.mac_ver(),
        )

    def __del__(self):
        self.isKill = True
        if self.connector:
            self.connector.destroy()
        if self.consoleDevices:
            self.consoleDevices.destroy()
        if self.cardReader:
            self.cardReader.destroy()
        self.connector = None
        self.consoleDevices = None
        self.cardReader = None

#
class LogService(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        self.reconectLog()

    def reconectLog(self):
        print("start logging")
        while(True):
            MY_LOGGER.initLogger()
            time.sleep(10)
            MY_LOGGER.closeLogger()
            time.sleep(1)


# interrupted by signal 9: SIGKILL

import signal
import sys

main = None
checkerService = None

def signal_hanlder(signal = None, frame = None):
    print("signal handler")

    main.terminate()
    time.sleep(1)
    sys.exit(0)

if __name__ == '__main__':
    # service = LogService()
    # service.start()

    killCopyService = KillCopyService()
    killCopyService.check()

    from src.updater import Updater
    isUpdated = Updater.runUpdate()

    if isUpdated == False :
        printInfo("version consoleService:" + Updater.APP_VERSION)
        signal.signal(signal.SIGINT, signal_hanlder)

        main = Main()
        main.start()

        # signal.pause()