import sys

import datetime

from src.com import Config


class LogTerminal(object):
    def __init__(self):
        # logFile = Config.getFolder() + "console_service.log"
        logFile = "console_service.log"
        self.terminal = sys.stdout
        self.log = open(logFile, "a")

    def write(self, message):
        self.terminal.write("LOG::" + message)
        self.log.write("LOG::" + message)

    def closeTerminal(self):
        print("LOG::closeTerminal")
        self.terminal.flush()
        self.log.close()
        del self.log
        del self.terminal

class Logger:
    def __init__(self):
        self.logger = None

    def initLogger(self):
        self.logger = LogTerminal()
        sys.stdout = self.logger
        sys.stderr = self.logger
        now = datetime.datetime.now()
        print("--------" + now.strftime("%H:%M:%S") + "--------");

    def closeLogger(self):
        if(self.logger):
            self.logger.closeTerminal()
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__
        self.logger = None


MY_LOGGER = Logger()