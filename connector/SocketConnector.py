import socket
import threading

import sys
from time import sleep

from src.utils.debug import printInfo


class SocketConnector():
    def __init__(self, threads):
        # self.__sock = socket.socket()
        self.__conn = None
        self.__addr = None
        self.__callback = None
        self.port = 2020
        self.isConnected = False
        self.threads = threads
        self.isRunning = True

    def init(self, port, callback):
        """
        create socket at computer
        create thread to connect operation (becouse console device work at main thread)
        """
        print "init socket, port:", port
        self.port = port
        self.__sock = self.__getSock(port)
        # self.__sock.settimeout(3000)
        self.__callback = callback

        tFlashConnector = threading.Thread(target=self.__connect, name="flashConnectorThread")
        self.threads.append(tFlashConnector)
        tFlashConnector.start()

    def __getSock(self, port):
        for res in socket.getaddrinfo("localhost", port, socket.AF_UNSPEC,
                                      socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
            af, socktype, proto, canonname, sa = res
            try:
                s = socket.socket(af, socktype, proto)
            except socket.error as msg:
                s = None
                continue
            try:
                s.bind(sa)
                s.listen(1)
                print("binding socket:", sa)
                return s
            except socket.error as msg:
                s.close()
                s = None
                continue

    def __connect(self):
        print("FlashSocket::connect", self.isRunning)
        while self.isRunning:
            try:
                self.__conn, self.__addr = self.__sock.accept()
                self.isConnected = True
            except:
                print("Flash socket timeout except")
                self.__conn = None
                self.__addr = None
                self.isConnected = False
                sleep(1)
            while self.isRunning and self.__conn <> None and self.isConnected:
                try:
                    data = self.__conn.recv(1024)
                    res = data.decode("utf-8")
                    self.__socketHandler(res)
                    if not data:
                        break
                except:
                    self.__conn = None
                    print("Unexpected error LOBBY:", sys.exc_info()[0])
                    break

    def __socketHandler(self, value):
        # transport models from flash to console
        print("get:", value)
        self.__callback(value)

    def sendData(self, value):
        print("send:", self.isConnected, value)
        if self.isConnected and self.__conn:
            self.__conn.send(b"" + value + "")
            print("send:", value)

    def transportFromConsoleToFlash(self, value):
        self.sendData(value)
        # if value:
        #     self.sendData("errorDevice")
        # else:
        #     self.sendData("stableDevice")

    def destroy(self):
        printInfo("Wait to close Socket")
        self.isRunning = False
        self.isConnected = False
        if self.__conn is not None:
            self.__conn.close()

        self.__sock = None
        self.__conn = None
        self.__addr = None
        self.__callback = None
