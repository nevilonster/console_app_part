
CHANGE_BUTTONS = "changeButtons"
ACTIVATE_CONNECTION = "activateConnection"
DEACTIVATE_CONNECTION = "deactivateConnection"

KEY_DELIMITER = "_______"

CHANGE_STATE_CARD_AT_CONSOLE = "changeStateCardAtConsole"

# ---------------technical consts
TECHNICAL_CONNECTION = "technicalConnection"
SOUND_TECHNICAL_SETTINGS = "soundTechnicalSettings"
SYSTEM_TECHNICAL_SETTINGS = "systemTechnicalSettings"


#-------------system consts reader---------------
UPDATE_CARD_AT_READER_STATE = "updateCardAtReaderState"
RELOAD_READER_STATE = "reloadReaderState"
TERMINATE_THREAD = "terminateThread"

#-------------system console consts----------
RELOAD_CONSOLE_STATE = "reloadConsoleState"

#-------------CARD_ID_STATE----------------
INCORRECT_ID_CARD = -2 # problem with card
ABSENT_ID_CARD = -1    # card is absent at card reader